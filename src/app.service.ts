import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return `Hello World! EXAMPLE_VAR: ${process.env.EXAMPLE_VAR}, THE_SECRET: ${process.env.THE_SECRET} new version`;
  }
}

import { Controller, Get } from '@nestjs/common';

@Controller('user')
export class UserController {
  @Get()
  getHello(): any {
    return {
      user: 'Foo',
      address: 'BKK',
    };
  }
}

import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TerminusModule } from '@nestjs/terminus';
import { HealthModule } from './health/health.module';
import { UserModule } from './user/user.module';
import { CatModule } from './cat/cat.module';
@Module({
  imports: [TerminusModule, HealthModule, UserModule, CatModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

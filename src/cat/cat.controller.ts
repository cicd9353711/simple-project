import { Controller, Get } from '@nestjs/common';

@Controller('cat')
export class CatController {
  private state = 0;
  @Get()
  getHello(): any {
    this.state += 1;
    return {
      name: 'Kitty',
      address: this.state,
    };
  }
}

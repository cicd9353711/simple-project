###################
# BUILD FOR LOCAL DEVELOPMENT
###################

FROM node:18.15.0-alpine As development

RUN apk add --update tzdata
ENV TZ=Asia/Bangkok
RUN rm -rf /var/cache/apk/*

USER node

WORKDIR /home/node/app

COPY --chown=node:node package.json ./

COPY --chown=node:node yarn.lock .

RUN yarn install

COPY --chown=node:node . .

###################
# BUILD FOR PRODUCTION
###################

FROM node:18.15.0-alpine As build

USER node

WORKDIR /home/node/app

COPY --chown=node:node package.json ./

COPY --chown=node:node yarn.lock .

COPY --chown=node:node --from=development /home/node/app/node_modules ./node_modules

COPY --chown=node:node . .

RUN yarn install --only=production && yarn cache clean --force

RUN yarn build

ENV NODE_ENV production

###################
# PRODUCTION
###################

FROM node:18.15.0-alpine As production

RUN apk add --update tzdata
ENV TZ=Asia/Bangkok
RUN rm -rf /var/cache/apk/*

USER node

# Copy the bundled code from the build stage to the production image
COPY --chown=node:node --from=build /home/node/app/node_modules ./node_modules
COPY --chown=node:node --from=build /home/node/app/dist ./dist

# Start the server using the production build
CMD [ "node", "dist/main.js" ]
